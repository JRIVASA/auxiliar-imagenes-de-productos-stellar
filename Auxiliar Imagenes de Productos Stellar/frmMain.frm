VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Insertar Imagenes"
   ClientHeight    =   7440
   ClientLeft      =   150
   ClientTop       =   540
   ClientWidth     =   8835
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   8835
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar pb 
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   6840
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
      Scrolling       =   1
   End
   Begin MSComctlLib.ListView lvimg 
      Height          =   7095
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   12515
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      Icons           =   "imglistv"
      SmallIcons      =   "imglistv"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Producto"
         Object.Width           =   12348
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Archivo"
         Object.Width           =   2540
      EndProperty
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   7185
      Width           =   8835
      _ExtentX        =   15584
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12965
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            TextSave        =   "6/21/2018"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog cdialog 
      Left            =   8280
      Top             =   5520
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imglistv 
      Left            =   8160
      Top             =   4920
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":058A
            Key             =   "ok"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":0B24
            Key             =   "cancel"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmMain.frx":10BE
            Key             =   "save"
         EndProperty
      EndProperty
   End
   Begin VB.Image Image1 
      Height          =   3120
      Left            =   2760
      Picture         =   "frmMain.frx":1658
      Stretch         =   -1  'True
      Top             =   1800
      Width           =   3120
   End
   Begin VB.Menu mnu 
      Caption         =   "Menu"
      Begin VB.Menu buscar 
         Caption         =   "&Buscar Imagenes"
         Shortcut        =   {F2}
      End
      Begin VB.Menu importar 
         Caption         =   "&Guardar Imagenes"
         Shortcut        =   {F4}
      End
      Begin VB.Menu sep 
         Caption         =   "-"
      End
      Begin VB.Menu salir 
         Caption         =   "&Salir"
         Shortcut        =   {F12}
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private mEstadoLogo As eEstadoLogo
Private mEstadoForm As eEstadoForm
Private mUltimoEstadologo As eEstadoLogo
Private mImagenSel  As String

Private Sub CambiarTamano()
'    Select Case mEstadoLogo
'        Case eEstadoLogo.eEstadoMin
'            Image1.Height = 3120
'            Image1.Width = 3120
'
'        Case Else
'            Image1.Height = 3120 * (2 * mEstadoLogo)
'            Image1.Width = 3120 * (2 * mEstadoLogo)
'    End Select
'    CentrarCtrl Image1
End Sub

Private Sub CentrarCtrl(ByRef pCtrl As Object)
    Dim mCenterH As Long, mCenterV As Long
    
    mCenterH = Me.Width / 2
    mCenterV = Me.Height / 2 - Me.StatusBar1.Height
    
    pCtrl.Left = mCenterH - pCtrl.Width / 2
    pCtrl.Top = mCenterV - pCtrl.Height / 2
End Sub

Private Sub buscar_Click()
    Dim mCarpeta As String
    
    mCarpeta = Buscar_Carpeta("Ruta de las Imagenes", vbNullString)
    If mCarpeta <> "" And mCarpeta <> vbNullString Then
        CambiarEstadoForm efrmImportar
        BuscarImagenes mCarpeta
    Else
        CambiarEstadoForm efrmEstandar
    End If
End Sub

Private Sub Form_Load()
    IniciarForm
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Cancel = 1
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If mEstadoLogo <> gAppConfig.EstadoLogo Then
        gApi.EscribirConfiguracion gAppConfig.ArchivoIni, "Config", "EstadoLogo", CStr(mEstadoLogo)
    End If
End Sub

Private Sub Image1_DblClick()
    mEstadoLogo = IIf(mEstadoLogo = eEstadoMax, 0, mEstadoLogo + 1)
    CambiarTamano
End Sub

Function Buscar_Carpeta(Optional Titulo As String, _
Optional Path_Inicial As Variant) As String
    
    On Local Error GoTo errFunction
    
    Dim objShell As Object
    Dim objFolder As Object
    Dim o_Carpeta As Object
      
    ' Nuevo objeto Shell.Application
    Set objShell = CreateObject("Shell.Application")
      
    On Error Resume Next
    'Abre el cuadro de di�logo para seleccionar
    Set objFolder = objShell.BrowseForFolder( _
                            0, _
                            Titulo, _
                            0, _
                            Path_Inicial)
      
    ' Devuelve solo el nombre de carpeta
    Set o_Carpeta = objFolder.Self
      
    ' Devuelve la ruta completa seleccionada en el di�logo
    Buscar_Carpeta = o_Carpeta.Path
  
    Exit Function
    
'Error
errFunction:
    
    MsgBox Err.Description, vbCritical
    Buscar_Carpeta = vbNullString
  
End Function

Private Sub IniciarForm()
    pb.Visible = False
    CambiarEstadoForm efrmEstandar
    mEstadoLogo = gAppConfig.EstadoLogo
    CambiarTamano
End Sub

Private Sub CambiarEstadoForm(pEstado As eEstadoForm)
    If pEstado = efrmMostrar Then
        mUltimoEstadologo = mEstadoLogo
    ElseIf pEstado = efrmImportar And mEstadoForm = efrmMostrar Then
        mEstadoLogo = mUltimoEstadologo
        CambiarTamano
    End If
    mEstadoForm = pEstado
    importar.Visible = mEstadoForm = efrmImportar
    buscar.Visible = mEstadoForm = efrmEstandar
    salir.Caption = IIf(mEstadoForm = efrmEstandar, "&Salir", "&Volver")
    lvimg.Visible = mEstadoForm = efrmImportar
    If mEstadoForm <> efrmImportar Then
        Image1.Visible = True
        On Error GoTo Error
        Image1.Picture = LoadPicture
        Image1.Picture = LoadPicture(BuscarImagenSel)
Error:
        If Err.Number <> 0 Then
            MsgBox "Im�gen inv�lida o no se pudo cargar." & vbNewLine & vbNewLine & "Informaci�n adicional del error: " & Err.Description & " " & "(" & Err.Number & ")"
        End If
    Else
        Image1.Visible = False
    End If
End Sub

Private Function BuscarImagenSel() As String
    
    If mEstadoForm = efrmEstandar Then
        If Dir(App.Path & "\logo.jpg", vbArchive) <> "" Then
            BuscarImagenSel = "logo.jpg"
            Exit Function
        End If
        If Dir(App.Path & "\logo.bmp", vbArchive) <> "" Then
            BuscarImagenSel = "logo.bmp"
            Exit Function
        End If
    Else
        BuscarImagenSel = mImagenSel
    End If
    
End Function

Private Sub importar_Click()
    
    Dim mItm As ListItem
    Dim mIni As Boolean
    
    On Error GoTo Errores
    
    If lvimg.ListItems.Count = 0 Then Exit Sub
    
    If CrearConexion() Then
        pb.Visible = True
        pb.Value = 0
        pb.Max = lvimg.ListItems.Count + 1
        gApi.ProgressbarInStatusBarApi pb, Me.StatusBar1, 1
        
        'gAppConfig.Conexion.BeginTrans: mIni = True
        If MsgBox("Esta Seguro de Guardar las Imagenes", vbYesNo) = vbYes Then
            For Each mItm In lvimg.ListItems
                pb.Value = pb.Value + 1
                If mItm.Tag <> "" Then
                    mItm.SmallIcon = GrabarImagen(gAppConfig.Conexion, Mid(mItm.Key, 2, Len(mItm.Key)), mItm.Tag)
                End If
            Next
        End If
        'gAppConfig.Conexion.CommitTrans: mIni = False
        MsgBox "PROCESO CULMINADO", vbInformation
        pb.Visible = False
    
    End If
    Exit Sub
Errores:
    'If mIni Then gAppConfig.Conexion.RollbackTrans
    EscribirLog "Error (" & Err.Number & ") " & Err.Description, "Grabando Imagenes"
    MsgBox Err.Description, vbCritical
    pb.Visible = False
End Sub

Private Sub lvimg_DblClick()
    mImagenSel = lvimg.SelectedItem.Tag
    If mImagenSel <> "" Then
        CambiarEstadoForm efrmMostrar
    End If
End Sub

Private Sub salir_Click()
    If mEstadoForm = efrmEstandar Then
        Unload Me
    Else
        CambiarEstadoForm mEstadoForm - 1
    End If
End Sub

Private Function BuscarImagenes(pCarpeta As String)
    Dim mRs As ADODB.Recordset
    Dim mArchivo As String
    Dim misExt As Variant
    Dim mItm As ListItem
    
    On Error GoTo Errores
    lvimg.ListItems.Clear
    If CrearConexion() Then
        misExt = Split(gAppConfig.Extensiones, "|")
        For i = 0 To UBound(misExt)
            mArchivo = Dir(pCarpeta & "\*." & misExt(i), vbArchive)
            Do While mArchivo <> ""
                Set mRs = BuscarProducto(mArchivo)
                If Not mRs.EOF Then
                    Set mItm = lvimg.ListItems.Add(, , mRs!c_descri, , 1)
                    mItm.Tag = pCarpeta & "\" & mArchivo
                    mItm.Key = "K" & mRs!c_codigo
                Else
                    Set mItm = lvimg.ListItems.Add(, , "No se Encontro el producto", , 2)
                End If
                mItm.SubItems(1) = mArchivo
                mArchivo = Dir
            Loop
        Next
    End If
    If lvimg.ListItems.Count = 0 Then CambiarEstadoForm efrmEstandar
    Exit Function
Errores:
    EscribirLog "Error (" & Err.Number & ") " & Err.Description, "Buscando Imagenes"
    MsgBox Err.Description, vbCritical
    
End Function

Private Function BuscarProducto(pCodigo As String) As ADODB.Recordset
    Dim mRs As ADODB.Recordset
    Dim mSql As String
    Dim mCod
    
    mCod = Split(pCodigo, ".")
    
    mSql = "Select p.c_codigo,p.c_descri from ma_productos p inner join ma_codigos c on p.c_codigo=c.c_codnasa and c.c_codigo='" & mCod(0) & "' "
    Set mRs = New ADODB.Recordset
    mRs.CursorLocation = adUseClient
    mRs.Open mSql, gAppConfig.Conexion, adOpenForwardOnly, adLockReadOnly, adCmdText
    Set BuscarProducto = mRs
    
End Function

Private Function GrabarImagen(pCn As ADODB.Connection, pCodigo As String, pImagen As String) As Integer
    Dim mRs As ADODB.Recordset
    Dim mSql As String
    
    mSql = "Select * from ma_productos where c_codigo='" & pCodigo & "' "
    Set mRs = New ADODB.Recordset
    mRs.Open mSql, pCn, adOpenKeyset, adLockPessimistic, adCmdText
    If Not mRs.EOF Then
        If gAppConfig.MetodoImagen = 1 Then
            If Guardar_Imagen(mRs.Fields("c_fileimagen"), pImagen) Then
                mRs.Update
                GrabarImagen = 3
                Exit Function
            End If
        Else
            If PushImageToDb(mRs.Fields("c_fileimagen"), pImagen) Then
                mRs.Update
                GrabarImagen = 3
                Exit Function
            End If
        End If
    End If
    GrabarImagen = 2
End Function

Public Function GuardarBinary(ADOField As ADODB.Field, pImagen As String) As Boolean
    ' Guardar el contenido del Picture en el campo de la base
    Dim i As Long
    Dim Fragment As Long
    Dim nSize As Long
    Dim nChunks As Long
   
    On Error GoTo Errores
    ' Leer el fichero y guardarlo en el campo
    nFile = FreeFile
    Open pImagen For Binary Access Read As nFile
    nSize = LOF(nFile)    ' Longitud de los datos en el archivo
    If nSize = 0 Then
        Close nFile
        Exit Function
    End If
    '
    ' Calcular el n�mero de trozos y el resto
    nChunks = nSize \ mBuffer
    Fragment = nSize Mod mBuffer
    ReDim Chunk(Fragment)
    '
    Get nFile, , Chunk()
    ADOField.AppendChunk Chunk()
    ReDim Chunk(mBuffer)
    For i = 1 To nChunks
        Get nFile, , Chunk()
        ADOField.AppendChunk Chunk()
    Next i
    Close nFile
    GuardarBinary = True
    Exit Function
Errores:
    EscribirLog "Error (" & Err.Number & ") " & Err.Description, "GuardarBinary"
    Err.Clear
End Function

Function PushImageToDb(pAdoField As ADODB.Field, pImagen As String) As Boolean
    Dim LibreArc As Integer, Buffer As Variant
    Dim FS, f, s, counter As Long
    
    On Error GoTo Errores
    
    'VERIFICA SI EXISTE PARA BORRARLO ANTES DE EMPEZAR
    stemp = pImagen
    Set FS = CreateObject("Scripting.FileSystemObject")
    If Not FS.fileexists(pImagen) Then
        Exit Function
    End If
    LibreArc = FreeFile()
    FilTam = FileLen(pImagen)
    Open pImagen For Binary Access Read As #LibreArc
    Buffer = Input(LOF(LibreArc), LibreArc)
    pAdoField.AppendChunk (Buffer)
    'RecSet.UpdateBatch
    Close LibreArc
    PushImageToDb = True
    Exit Function
Errores:
    EscribirLog "Error (" & Err.Number & ") " & Err.Description, "GuardarBinary"
    'VERIFICA SI EXISTE PARA BORRARLO AL TERMINAR
    
End Function

Public Function Guardar_Imagen(pAdoField As ADODB.Field, Optional Path_Imagen As String) As Boolean
  
    Dim Stream As ADODB.Stream
    
    On Error GoTo Error_Sub
    
    'Nuevo objeto ADODB Stream
    Set Stream = New ADODB.Stream
      
    ' dato de tipo binario
    Stream.Type = adTypeBinary
      
    Stream.Open
        ' verifica que la ruta del gr�fico no sea una cadena vac�a
        If Len(Path_Imagen) <> 0 Then
              
            ' lee la imagen desde el path
            Stream.LoadFromFile Path_Imagen
            ' La guarda en campo
            pAdoField.Value = Stream.Read
              
            ' actualiza los cambios
            
        End If
   
    If Stream.State = adStateOpen Then
        Stream.Close
    End If
    If Not Stream Is Nothing Then
        Set Stream = Nothing
    End If
    ' Retorno
    Guardar_Imagen = True
      
Exit Function
  
Error_Sub:
  If Err.Number <> 0 Then
    EscribirLog "Error (" & Err.Number & ") " & Err.Description, "GuardarBinary"
  End If
    
End Function

