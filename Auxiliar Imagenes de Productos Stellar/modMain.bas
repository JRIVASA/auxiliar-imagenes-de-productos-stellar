Attribute VB_Name = "modMain"
Type EstrucApp
    Conexion                                                        As ADODB.Connection
    ArchivoIni                                                      As String
    ArchivoLog                                                      As String
    Servidor                                                        As String
    Catalogo                                                        As String
    Usuario                                                         As String
    Password                                                        As String
    Extensiones                                                     As String
    MetodoImagen                                                    As Integer
    CommandTimeOut                                                  As Long
    ConnectionTimeOut                                               As Long
    EstadoLogo                                                      As eEstadoLogo
End Type

Enum eEstadoLogo
    eEstadoMin
    eEstadoNor
    eEstadoMax
End Enum

Enum eEstadoForm
    efrmEstandar
    efrmImportar
    efrmMostrar
End Enum
    
Public gAppConfig                                                   As EstrucApp
Public gApi                                                         As clsApi

Sub main()
    Set gApi = New clsApi
    With gAppConfig
        .ArchivoIni = App.Path & "\Config.ini"
        .ArchivoLog = App.Path & "\AppLog.txt"
        .Servidor = gApi.ObtenerConfiguracion(.ArchivoIni, "Server", "Servidor", "(local)")
        .Catalogo = gApi.ObtenerConfiguracion(.ArchivoIni, "Server", "BD", "VAD10")
        .CommandTimeOut = Val(gApi.ObtenerConfiguracion(.ArchivoIni, "Server", "CommandTimeOut", "20"))
        .ConnectionTimeOut = Val(gApi.ObtenerConfiguracion(.ArchivoIni, "Server", "ConnectionTimeOut", "10"))
        .EstadoLogo = Val(gApi.ObtenerConfiguracion(.ArchivoIni, "Config", "EstadoLogo", "0"))
        .Password = gApi.ObtenerConfiguracion(.ArchivoIni, "Server", "PWD", "")
        .Usuario = gApi.ObtenerConfiguracion(.ArchivoIni, "Server", "User", "sa")
        .MetodoImagen = gApi.ObtenerConfiguracion(.ArchivoIni, "Config", "MetodoImagen", "0")
        .Extensiones = gApi.ObtenerConfiguracion(.ArchivoIni, "Config", "Extension", "jpg|bmp|gif")
    End With
    frmMain.Show vbModal
    End
End Sub

Public Function CrearConexion() As Boolean
    On Error GoTo Errores
    With gAppConfig
        Set .Conexion = New ADODB.Connection
        .ConnectionTimeOut = .ConnectionTimeOut
        .CommandTimeOut = .CommandTimeOut
        .Conexion.Open "provider=sqloledb.1;initial catalog=" & .Catalogo & ";user id=" & .Usuario & ";password=" & .Password & ";data source=" & .Servidor
        CrearConexion = True
    End With
    Exit Function
Errores:
    EscribirLog "Error (" & Err.Number & ") " & Err.Description, "Creando Conexion"
    MsgBox Err.Description, vbCritical
    Err.Clear
End Function

Public Sub EscribirLog(pCadena As String, Optional pRutina As String = "")
   Dim mCanal As Integer
   
   mCanal = FreeFile()
   Open gAppConfig.ArchivoLog For Append Access Write As #mCanal
   Print #mCanal, Now & ", " & pCadena & IIf(pRutina <> "", ", Rutina " & pRutina, "")
   Close #mCanal
        
End Sub
