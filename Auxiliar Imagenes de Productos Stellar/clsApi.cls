VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsApi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
' para manejo de .ini
Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpDefault _
    As String, ByVal lpReturnedString As String, ByVal _
    nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName _
    As String, ByVal lpKeyName As Any, ByVal lpstring _
    As Any, ByVal lpFileName As String) As Long
    
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
(ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, _
ByVal lpParameters As String, ByVal lpDirectory As String, _
ByVal nShowCmd As Long) As Long

Private Declare Function SendMessage Lib "user32" Alias _
                        "SendMessageA" (ByVal hwnd As Long, _
                         ByVal wMsg As Long, ByVal wParam As Long, _
                         lParam As Long) As Long
                         
Private Declare Function SetParent Lib "user32" _
        (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
  
'Obtener Configuracion Regional
Private Declare Function GetUserDefaultLCID Lib "kernel32" () As Long
     
Private Declare Function GetLocaleInfo Lib "kernel32" Alias "GetLocaleInfoA" ( _
        ByVal Locale As Long, _
        ByVal LCType As Long, _
        ByVal lpLCData As String, _
        ByVal cchData As Long) As Long

' Manejo de Archivos
Private Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal bFailIfExists As Long) As Long
Private Declare Function CreateDirectory Lib "kernel32" Alias "CreateDirectoryA" (ByVal lpPathName As String, lpSecurityAttributes As Long) As Long
Private Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" (ByVal lpFileName As String) As Long
Private Declare Function GetFileSize Lib "kernel32" (ByVal hFile As Long, lpFileSizeHigh As Long) As Long
Private Declare Function GetFileTime Lib "kernel32" (ByVal hFile As Long, lpCreationTime As FILETIME, lpLastAccessTime As FILETIME, lpLastWriteTime As FILETIME) As Long
Private Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long
Private Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, ByVal dwShareMode As Long, lpSecurityAttributes As Long, ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, ByVal hTemplateFile As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Long) As Long
Private Declare Function SHFileOperation Lib "shell32.dll" Alias "SHFileOperationA" (lpFileOp As SHFILEOPSTRUCT) As Long
Private Declare Function FileTimeToSystemTime Lib "kernel32" (lpFileTime As FILETIME, lpSystemTime As SYSTEMTIME) As Long
Private Declare Function FileTimeToLocalFileTime Lib "kernel32" (lpFileTime As FILETIME, lpLocalFileTime As FILETIME) As Long

' SendMessage
Private Const CB_SETDROPPEDWIDTH = &H160

'**********************************
Private Type FILETIME
    dwLowDateTime As Long
    dwHighDateTime As Long
End Type

Private Type SHFILEOPSTRUCT
    hwnd As Long
    wFunc As Long
    pFrom As String
    pTo As String
    fFlags As Integer
    fAborted As Boolean
    hNameMaps As Long
    sProgress As String
End Type

Private Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

' Manejo Formularios (Redondeo,Transparecia,Circular)

Private Declare Function CreateRoundRectRgn Lib "gdi32" ( _
    ByVal X1 As Long, _
    ByVal Y1 As Long, _
    ByVal X2 As Long, _
    ByVal Y2 As Long, _
    ByVal X3 As Long, _
    ByVal Y3 As Long) As Long
  
'Establece la región
Private Declare Function SetWindowRgn Lib "user32" ( _
    ByVal hwnd As Long, _
    ByVal hRgn As Long, _
    ByVal bRedraw As Boolean) As Long
    
Private Declare Function CreateEllipticRgn Lib "gdi32" ( _
        ByVal X1 As Long, _
        ByVal Y1 As Long, _
        ByVal X2 As Long, _
        ByVal Y2 As Long) As Long
        
' Recuperar IP
Private Type IPINFO
    dwAddr As Long ' Dirección Ip
    dwIndex As Long
    dwMask As Long
    dwBCastAddr As Long
    dwReasmSize As Long
    unused1 As Integer
    unused2 As Integer
End Type
  
Private Type MIB_IPADDRTABLE
    dEntrys As Long 'Numero de entradas de la tabla
    mIPInfo(5) As IPINFO 'Array de entradas de direcciones Ip
End Type
  
Private Type IP_Array
    mBuffer As MIB_IPADDRTABLE
    BufferLen As Long
End Type
  
'Función Api CopyMemory
Private Declare Sub CopyMemory _
    Lib "kernel32" _
    Alias "RtlMoveMemory" ( _
        Destination As Any, _
        Source As Any, _
        ByVal Length As Long)
  
'Función Api GetIpAddrTable para obtener la tabla de direcciones IP
Private Declare Function GetIpAddrTable _
    Lib "IPHlpApi" ( _
    pIPAdrTable As Byte, _
    pdwSize As Long, _
    ByVal Sort As Long) As Long

' Recuperar Nombre Equipo
Private Declare Function GetComputerName Lib "kernel32" _
Alias "GetComputerNameA" (ByVal lpBuffer As String, nSize As _
Long) As Long

' Constantes

Private Const FO_MOVE As Long = &H1
Private Const FO_COPY As Long = &H2
Private Const FO_DELETE = &H3
Private Const FO_RENAME As Long = &H4
Private Const GENERIC_WRITE = &H40000000
Private Const GENERIC_READ = &H80000000
Private Const OPEN_EXISTING = 3
Private Const FILE_SHARE_READ = &H1
Private Const FILE_SHARE_WRITE = &H2

Private Const NUMBER_ERROR = 1526897
Private Const SHGFI_DISPLAYNAME = &H200
Private Const SHGFI_TYPENAME = &H400
Private Const MAX_PATH = 260
Private Const CSIDL_DESKTOP = &H0
Private Const CSIDL_PROGRAMS = &H2
Private Const CSIDL_CONTROLS = &H3
Private Const CSIDL_PRINTERS = &H4
Private Const CSIDL_PERSONAL = &H5
Private Const CSIDL_FAVORITES = &H6
Private Const CSIDL_STARTUP = &H7
Private Const CSIDL_RECENT = &H8
Private Const CSIDL_SENDTO = &H9
Private Const CSIDL_BITBUCKET = &HA
Private Const CSIDL_STARTMENU = &HB
Private Const CSIDL_DESKTOPDIRECTORY = &H10
Private Const CSIDL_DRIVES = &H11
Private Const CSIDL_NETWORK = &H12
Private Const CSIDL_NETHOOD = &H13
Private Const CSIDL_FONTS = &H14
Private Const CSIDL_TEMPLATES = &H15

Private Const SHOW_OPENWINDOW = 1
' metodos

Public Sub Redondear_Formulario(El_Form As Form, Radio As Long)
    Dim Region As Long, Ret As Long, Ancho As Long
    Dim Alto As Long, old_Scale As Integer
       
    ' guardar la escala
    old_Scale = El_Form.ScaleMode
       
    ' cambiar la escala a pixeles
    El_Form.ScaleMode = vbPixels
       
    'Obtenemos el ancho y alto de la region del Form
    Ancho = El_Form.ScaleWidth
    Alto = El_Form.ScaleHeight
  
    'Pasar el ancho alto del formualrio y el valor de redondeo .. es decir el radio
    Region = CreateRoundRectRgn(0, 0, Ancho, Alto, Radio, Radio)
  
    ' Aplica la región al formulario
    Ret = SetWindowRgn(El_Form.hwnd, Region, True)
       
    ' restaurar la escala
    El_Form.ScaleMode = old_Scale
    
    ' llamada Call Redondear_Formulario(Me, 100)
End Sub
  
'Función para Convertir el valor de tipo Long a un string
Private Function ConvertirDirecciónAstring(longAddr As Long) As String
    Dim myByte(3) As Byte 'array de tipo Byte
    Dim Cnt As Long
       
    CopyMemory myByte(0), longAddr, 4
    For Cnt = 0 To 3
        ConvertirDirecciónAstring = ConvertirDirecciónAstring + CStr(myByte(Cnt)) + "."
    Next Cnt
    ConvertirDirecciónAstring = Left$(ConvertirDirecciónAstring, Len(ConvertirDirecciónAstring) - 1)
End Function
  
'Función que retorna un string con la dirección Ip final
Public Function RecuperarIP() As String
  
    Dim Ret As Long, Tel As Long
    Dim bBytes() As Byte
    Dim TempList() As String
    Dim TempIP As String
    Dim Tempi As Long
    Dim Listing As MIB_IPADDRTABLE
    Dim L3 As String
  
On Error GoTo ErrSub
       
    GetIpAddrTable ByVal 0&, Ret, True
  
    If Ret <= 0 Then Exit Function
    ReDim bBytes(0 To Ret - 1) As Byte
    ReDim TempList(0 To Ret - 1) As String
  
    'recuperamos la tabla con las ip
    GetIpAddrTable bBytes(0), Ret, False
    CopyMemory Listing.dEntrys, bBytes(0), 4
  
    For Tel = 0 To Listing.dEntrys - 1
        'Copiamos la estructura entera a la lista
        CopyMemory Listing.mIPInfo(Tel), bBytes(4 + (Tel * Len(Listing.mIPInfo(0)))), Len(Listing.mIPInfo(Tel))
        TempList(Tel) = ConvertirDirecciónAstring(Listing.mIPInfo(Tel).dwAddr)
    Next Tel
  
    TempIP = TempList(0)
    For Tempi = 0 To Listing.dEntrys - 1
        L3 = Left(TempList(Tempi), 3)
        If L3 <> "169" And L3 <> "127" And L3 <> "192" Then
            TempIP = TempList(Tempi)
        End If
    Next Tempi
       
    RecuperarIP = TempIP
  
Exit Function
ErrSub:
  
RecuperarIP = ""
  
End Function

Public Function ObtenerNombreEquipo() As String
    Dim sEquipo As String * 255
    
    GetComputerName sEquipo, 255
    ObtenerNombreEquipo = Replace(sEquipo, Chr(0), "")
End Function

Public Function MoverCopiarArchivo(pOrigen As String, pDestino As String, Optional pEsMover As Boolean = False)
    If pEsMover Then
        CopiarArchivo = MoveFile(pOrigen, pDestino)
    Else
        CopiarArchivo = CopyFile(pOrigen, pDestino, 0)
    End If
End Function

Public Function ObtenerConfiguracion(SIniFile As String, SSection As String, SKey _
        As String, sdefault As String) As String
    Dim stemp As String * 256
    Dim nlength As Integer
    
    stemp = Space$(256)
    nlength = GetPrivateProfileString(SSection, SKey, sdefault, stemp, 255, SIniFile)
    ObtenerConfiguracion = Left$(stemp, nlength)
End Function

Public Function EscribirConfiguracion(SIniFile As String, SSection As String, SKey _
        As String, svalue As String) As String

    Dim stemp As String
    Dim n As Integer

    stemp = svalue
    n = WritePrivateProfileString(SSection, SKey, svalue, SIniFile)
End Function

Public Function AbrirNotePad(pHwnd As Long, pArchivo As String) As Long
    EjecutarAplicacion = ShellExecute(pHwnd, vbNullString, pArchivo, vbNullString, "", SHOW_OPENWINDOW)
End Function

Public Sub Establecer_Ancho(El_Combo As Object, El_Form As Form)
    Dim k As Integer, Escala As Byte
    Dim Maximo As Long
' Para guardar el item mas ancho del combo


'Guarda la escala del form para luego reestablecerla
    Escala = El_Form.ScaleMode
    
    'Cambia la escala
    El_Form.ScaleMode = vbPixels
    
    'Recorre los elementos para obtener el mas ancho
    For k = 0 To El_Combo.ListCount - 1
        
        If Maximo < El_Form.TextWidth(El_Combo.List(k)) Then
            Maximo = El_Form.TextWidth(El_Combo.List(k))
        End If
    
    Next

    'Aplica el cambio pasandole el hwnd del combo y el valor
    SendMessage El_Combo.hwnd, CB_SETDROPPEDWIDTH, Maximo + 12, 0
    
    'Reestablece la escala del form
    El_Form.ScaleMode = Escala

End Sub

Public Function ObtenerConfiguracionRegional(Valor As Long) As String
     
   Dim Simbolo As String
     
   Dim r1 As Long
   Dim r2 As Long
   Dim p As Integer
   Dim Locale As Long
     
   Locale = GetUserDefaultLCID()
   r1 = GetLocaleInfo(Locale, Valor, vbNullString, 0)
     
   'buffer
   Simbolo = String$(r1, 0)
     
   'En esta llamada devuelve el símbolo en el Buffer
   r2 = GetLocaleInfo(Locale, Valor, Simbolo, r1)
     
   'Localiza el espacio nulo de la cadena para eliminarla
   p = InStr(Simbolo, Chr$(0))
     
   If p > 0 Then
      'Elimina los nulos
      ObtenerConfiguracionRegional = Left$(Simbolo, p - 1)
   End If
     
End Function

Public Sub ProgressbarInStatusBarApi(ByRef pPGB As Object, ByRef pSTB As Object, pPanel As Integer)
    SetParent pPGB.hwnd, pSTB.hwnd
    pPGB.Top = 40
    pPGB.Left = pSTB.Panels(pPanel).Left
    pPGB.Width = pSTB.Panels(pPanel).Width
    pPGB.Height = pSTB.Height - 60
    pPGB.Visible = True
End Sub

Public Sub ProgressbarInStatusBar(ByRef pPGB As Object, ByRef pSTB As Object, pPanel As Integer)
    Dim Marg As Single
    Dim Panel As Panel
    Dim PosY As Single
     
    Marg = pPGB.Parent.ScaleY(2, vbPixels, vbTwips)
     
    Set Panel = pSTB.Panels(pPanel)
    Panel.Bevel = sbrNoBevel
    PosY = pPGB.Parent.ScaleHeight - pSTB.Height
    pPGB.Move Panel.Left, _
                   (PosY + Marg), _
                   Panel.Width, _
                   (pSTB.Height - Marg)
     
    pPGB.ZOrder
End Sub
